require 'base64'
require 'json/jwt'
require 'openssl'
require 'sinatra'
require 'rack/oauth2'
require 'securerandom'

require_relative './lib'

enable :sessions

def code_challenge(code_verifier)
  Base64.urlsafe_encode64(
    OpenSSL::Digest::SHA256.digest(code_verifier)
  ).gsub(/=/, '')
end

def get_client(authorization_endpoint, token_endpoint)
  Rack::OAuth2::Client.new(
    identifier: settings.client_id,
    redirect_uri: settings.redirect_uri,
    host: settings.host,
    authorization_endpoint: authorization_endpoint,
    token_endpoint: token_endpoint,
  )
end

get '/' do
  erb :index
end

get '/endpoints' do
  session[:authorization_endpoint] = params['authorization_endpoint']
  session[:token_endpoint] = params['token_endpoint']
  redirect '/start'
end


get '/profile' do
  profile_config = get_profile_config params['profile_url']
  session[:authorization_endpoint] = profile_config[:authorization_endpoint]
  session[:token_endpoint] = profile_config[:token_endpoint]
  redirect '/start'
end

get '/start' do
  client = get_client session[:authorization_endpoint], session[:token_endpoint]

  session[:state] = SecureRandom.hex(32)
  session[:code_verifier] = SecureRandom.hex(32)
  session[:nonce] = SecureRandom.hex(16)

  authorization_uri = client.authorization_uri(
    scope: %w(draft),
    state: session[:state],
    nonce: session[:nonce],
    code_challenge: code_challenge(session[:code_verifier]),
    code_challenge_method: :S256,
  )

  redirect to(authorization_uri)
end

get '/callback' do
  client = get_client session[:authorization_endpoint], session[:token_endpoint]
  client.authorization_code = params['code']

  access_token = nil
  begin
    access_token = client.access_token! :body,
      code_verifier: session[:code_verifier]
  rescue Rack::OAuth2::Client::Error => e
    content_type :json
    response = {
      error: e.message
    }

    [400, response.to_json]
  end

  response = {
    token_endpoint_response: access_token.raw_attributes
  }

  begin
    response[:access_token_claims] = JSON::JWT.decode(access_token.raw_attributes[:access_token], :skip_verification)
  rescue
    # ignore
  end

  begin
    response[:refresh_token_claims] = JSON::JWT.decode(access_token.raw_attributes[:refresh_token], :skip_verification) if access_token.raw_attributes[:refresh_token]
  rescue
    # ignore
  end

  begin
    response[:id_token_claims] = JSON::JWT.decode(access_token.raw_attributes[:id_token], :skip_verification) if access_token.raw_attributes[:id_token]
  rescue
    # ignore
  end

  content_type :json
  response.to_json
end
