# Tokens Pls

A simply Sinatra application to handle OAuth2 code exchange with a Public OAuth2 Client.

Primarily designed for use with [IndieAuth](https://indieauth.spec.indieweb.org), but compliant with OAuth2 Public Clients.

For more details on motivation of the project, please read [the announce post](https://www.jvt.me/posts/2021/03/06/tokens-pls/).

## Usage

```sh
bundle config set --local path 'vendor/bundle'
bundle install
bundle exec rackup
# app is now running at http://localhost:9292
```
