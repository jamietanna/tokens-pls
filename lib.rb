require 'json'
require 'net/http'
require 'microformats'

def get(url)
  uri = URI.parse(url)
  http = Net::HTTP.new(uri.host, uri.port)
  http.use_ssl = true if uri.instance_of? URI::HTTPS
  request = Net::HTTP::Get.new(uri.request_uri)
  http.request(request)
end

def get_profile_config(profile_url)
  res = get(profile_url)

  parsed = Microformats.parse(res.body)
  {
    profile_url: profile_url,
    authorization_endpoint: parsed.rels['authorization_endpoint'][0],
    token_endpoint: parsed.rels['token_endpoint'][0],
  }
end
