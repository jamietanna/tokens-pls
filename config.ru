require_relative 'app'

configure do
  set :protection, :except => [:json_csrf]
end

configure :development do
  set :client_id, 'http://localhost:9292'
  set :redirect_uri, 'http://localhost:9292/callback'
  set :host, 'localhost'
end

configure :production do
  set :client_id, 'https://tokens-pls.fly.dev'
  set :redirect_uri, 'https://tokens-pls.fly.dev/callback'
  set :host, 'tokens-pls.fly.dev'
end

run Sinatra::Application
